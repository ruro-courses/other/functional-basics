#lang scheme/base
(require racket/list)

; С помощью уместных функций высшего порядка (map, foldl, foldr, filter, …)
; опишите функцию (task3 lst), принимающую непустой список из непустых
; подсписков чисел. Функция находит максимальное среди всех средних
; арифметических модулей элементов вложенных списков. Стандартные функции
; min и max в решении не использовать!

(define (task3 lst)
  (define (abs-mean e) (/ (foldl + 0.0 (map abs e)) (length e)))
  (define (not-standard-max a b) (if (> a b) a b))
  (foldl not-standard-max -inf.0 (map abs-mean lst)))

; Tests:
(require rackunit)

(check-equal? [task3 '((0))] 0.0)
(check-equal? [task3 '((0) (0))] 0.0)
(check-equal? [task3 '((0) (0) (-0.5))] 0.5)
(check-equal? [task3 '((0) (0) (0 0 0 -1))] 0.25)
(check-equal? [task3 '((0) (0) (0 0 0 3) (0 0 0 -3))] 0.75)
(check-equal? [task3 '((4 -10 1 1) (0) (-1))] 4.0)
; (Пример)
(check-equal? [task3 '((3 -9 1 1) (-4 2 1) (4 -1))] 3.5)
